var express = require('express');
var bodyParser = require('body-parser')
var app = express();
var server = require('http').Server(app)
var io = require('socket.io')(server, { pingTimeout: 30000 });
var path = require('path')
var mongoose = require('mongoose');
var port = process.env.PORT || 3631;
var router = express.Router();
var config = require('./config');

// mongoose.connect('mongodb://admin:OpreU7546Tu@3.133.68.38:27017/Definebysole', function(error){
// 	if(error){
// 		console.log(error)
// 	}else{
// 		console.log("connection successful");
// 	}
// },
// { useMongoClient: true })

mongoose.connect('mongodb://localhost:27017/Definebysole_old', function (error) {
	if (error) {
		console.log(error)
	} else {
		console.log("connection successful");
	}
}, { useMongoClient: true })

mongoose.Promise = global.Promise;

const ConversationServices = require('./app/services/conversation')
const MessageServices = require('./app/services/message')
const UserServices = require('./app/services/user')

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('superSecret', config.secret);

app.use(express.static(path.join(__dirname, 'public')))

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html')
})


app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

let users = []

function findUser(userId) {
	return users.find(user => user.userId == userId)
}

app.post('/new', async function (req, res) {
	const existingConversation = await ConversationServices.findOne({
		participants: {
			$all: req.body.participants
		}
	})
	if (!existingConversation) {
		const conversation = await ConversationServices.save(req.body)



		res.json(conversation)
	} else {
		res.json(existingConversation)
	}
})


io.on('connection', function (socket) {

	socket.on('new_connection', function (data) {
		users.push({
			userId: data.userId,
			socketId: socket.id
		})

		socket.emit('id_assigned', { id: socket.id })
	})

	socket.on('disconnect', function (data) {
		users = users.filter(user => user.socketId !== socket.id)
	})

	socket.on('sendMessage', async function (data) {

		const user = findUser(data.to)
		if (user) {
			socket.broadcast.to(user.socketId).emit('receivedMessage', data);
		}
		const message = await MessageServices.save(data)
	})

});


app.get('/conversations/:id/user/:userId/tradeId/:tradeId', async function (req, res) {
	const existingConversation = await ConversationServices.findOne({
		_id: req.params.id
	})
	const messages = await MessageServices.find({
		conversationId: req.params.id
	}) || []

	let unreadedMess = await MessageServices.find({ 
		// 'to': req.params.userId, 
		'conversationId': req.params.id, 
		readBy: {
			$ne: [req.params.userId]
		}
	})

		if (existingConversation) {
				if(messages.length > 0 && unreadedMess.length > 0){
						let newArray = []
						await unreadedMess.forEach(message => {
							newArray = message.readBy
							let checkArray = newArray.includes(req.params.userId)
							if (checkArray) {
								newArray = newArray
							}else{
								newArray.push(req.params.userId)
							}
							MessageServices.update({_id: message._id},{readBy: newArray})
						});
						const updatedMessages = await MessageServices.find({
							conversationId: req.params.id
						})
						res.json({ conversation: existingConversation || {}, messages: updatedMessages })
				} else {
					res.json({ conversation: existingConversation || {}, messages: messages})
				}
		}else{
			res.json({ message: 'no conversation found' })
		}
	
	// if (existingConversation) {
	// 	res.json({ conversation: existingConversation || {}, messages: messages })
	// } else {
	// 	res.json({ message: 'no conversation found' })
	// }
})




app.post('/checkUnreadMessage', async function (req, res) {
	const existingConversation = await ConversationServices.find({
		participants: {
			$in: req.body.tradeIdArray
		}
	})

	let conversationUnRead = []
	if (existingConversation) {
		for (const conversation of existingConversation) {
			const messages = await MessageServices.find({
				conversationId: conversation._id
			})
			console.log(messages,"messages messages messages")
			const UnReadMsgs = messages.filter(val => val.readBy.includes(req.body.userId) == false)
			console.log(UnReadMsgs,"UnReadMsgs UnReadMsgs UnReadMsgs")
			conversationUnRead.push({
				tradeId: conversation.participants[2],
				UnReadMsgs:UnReadMsgs.length
			})
		}
	}
	res.json({
		conversationUnRead: conversationUnRead || []
	})
})


app.get('/conversations/user/:userId', async function (req, res) {
	const existingConversation = await ConversationServices.find({
		participants: {
			$in: [req.params.userId]
		}
	})

	let conversationWithMessages = []

	if (existingConversation) {

		for (const conversation of existingConversation) {

			const messages = await MessageServices.find({
				conversationId: conversation._id
			})
			conversationWithMessages.push({
				...conversation._doc,
				lastMessage: messages[messages.length - 1]
			})
		}
	}
	res.json({
		conversations: conversationWithMessages || []
	})
})





app.get('/checkServer', async function (req, res) {
	res.json({ message: 'Server is Running' })
})

// app.listen(3631, () => {
//   console.log('Example app listening on port 3631!')
// });

// app.use('/api', router);

server.listen(port, () => {
	console.log('Example app listening on port 3631!')
});


  // app.get('/conversations/:id', async function (req, res) {
	// 	const existingConversation = await ConversationServices.findOne({
	// 		_id: req.params.id
	// 	})
	// 	const messages = await MessageServices.find({
	// 		conversationId: req.params.id
	// 	}) || []

	// 	if (existingConversation) {
  //         res.json({ conversation: existingConversation || {}, messages: messages})

	// 	}else{
	// 		res.json({ message: 'no conversation found' })
	// 	}
  // })
