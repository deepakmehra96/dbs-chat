const partial = (fn, ...firstArguments) => (...laterArguments) => fn(...firstArguments, ...laterArguments)

module.exports = {
    partial
}