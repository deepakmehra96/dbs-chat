const User = require('../schema/user')
const AsyncOperations = require('./AsyncOperations')

module.exports = new AsyncOperations(User)