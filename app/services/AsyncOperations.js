const { partial } = require('../utils/fnUtils')
const AsyncQuery = require('./AsyncQuery')

class AsyncOperations extends AsyncQuery {

    constructor (model) {
        super(model)

        this.find = partial(this.findQuery, 'find')
        this.findOne =  partial(this.findQuery, 'findOne')
        this.remove =  partial(this.remove, 'remove')
    }

    save (documentToSave) {
        return new Promise (
            (
                resolve, reject
            ) => {
    
                const newDocument = new this.model({
                    ...documentToSave
                })
    
                newDocument.save().then(
                    (document, err) => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(document)
                        }
                    }
                )
    
            }
        )
    }

    update (cond, documentToSave) {
        return new Promise (
            (
                resolve, reject
            ) => {
    
                const newDocument = {...documentToSave}
    
                this.model.update(cond, { $set: newDocument }).then(
                    (document, err) => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(document)
                        }
                    }
                )
    
            }
        )
    }

}

module.exports = AsyncOperations